
# Template Designing
### inside DocGen

---

### Template Designing

Designing an environment

that *enables* the CopyWriter  

to *easily* deliver dynamic content for their end users  

---

### What's Copywriting

'Filling the templates with human understandable content.'

---

### Copywriters: what to expect

- {:.fragment} layout through hard & soft linebreaks (`<p> vs <br>`)
- {:.fragment}... and even spaces (`&nbsp;`)
- {:.fragment} copy & paste (DRY only leaves you thirsty...)
- {:.fragment} rich text editing skills if you're lucky

---

### Deliveries

- {:.fragment} templates
- {:.fragment} datasets
    - {:.fragment} default
    - {:.fragment} combining
    - {:.fragment} common
    - {:.fragment} links

---

### Case

Too many templates, too many sections, too much logic

---

### Premailer

(http://premailer.dialect.ca/)

---

### Mailgun transactional email templates

(https://github.com/mailgun/transactional-email-templates)

---

### Access control through templates

Create custom templates for tasks!

---

### branching

publish x.y, patch to x.y.z

---

### mail subjects

- {:.fragment} Create a template *xxx_subject*, which has one section
- {:.fragment} include that section in a meta-tag in the actual mail template
- {:.fragment} extract the section using Nokogiri in your app

---

### required data

Do it upfront:

    %i(type brand locale).each{ |d| data(d, required: true) }
